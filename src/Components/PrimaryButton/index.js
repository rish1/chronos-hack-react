import React from 'react';

function PrimaryButton (props) {
    return (
        <div className="button-container">
            <button onClick={props.onClick} className={`button button-${props.type}`}>{props.children}</button>
        </div>
    );
}

export default PrimaryButton;