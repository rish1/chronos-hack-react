import React, { useState, useEffect } from 'react';
import PrimaryButton from '../../Components/PrimaryButton';

function Home(props) {

    const [clicked, setClicked] = useState(false);
    
    useEffect(() => {
        console.log("Listener activated when clicked is: ",clicked);
    },[clicked]);

    const handleClick = () => {
        console.log("On Clicked");
        setClicked(!clicked);
    };

    return (
        <div className="home">
            <h1 className="principal-heading">Welcome to React</h1>
            <PrimaryButton type="danger" onClick={() => handleClick()}>
                <p className="button-label">Get Started</p>
            </PrimaryButton>
        </div>
    );
}

export default Home;